
from hw17 import * 
from googlemaps import googlemaps
import argparse

def get_info_by_coordinate (latlon):
    gmaps = googlemaps.Client(key='AIzaSyBMU6prp1esf8Ezv5FxbZUI6fzorTb_L94')
    geocode_result = {}
    reverse_geocode_result = gmaps.reverse_geocode(latlon)[0]
    geocode_result['loc'] = reverse_geocode_result['formatted_address'] 
    geocode_result['lat'] = reverse_geocode_result['geometry']['location']['lat']
    geocode_result['lon'] = reverse_geocode_result['geometry']['location']['lng']
    return geocode_result


def file_type(filename):
    if not filename.endswith((".jpg",".jpeg")):
        raise argparse.ArgumentTypeError("Only .jpg or .jpeg files allowed")
    return filename


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="increase -i|--input filename.jpg", type=file_type)
args = parser.parse_args()
if args.input:
    print("location:", get_info_by_coordinate(get_exif_photo(args.input))['loc'])
    print('Google Maps URL: https://www.google.com/maps/search/?api=1&query={},{}'.format(get_info_by_coordinate(get_exif_photo(args.input))['lat'], get_info_by_coordinate(get_exif_photo(args.input))['lon']))
else:
    print('Please input correct filename, example: {} -i|--input filename.jpg|.jpeg'.format(parser.prog))