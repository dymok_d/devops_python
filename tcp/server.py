import socket, sys
from threading import Thread
import datetime


def wait_connection():
	while True:
		clientsocket, clientaddress = s.accept()
		print("Join chat =>:", clientaddress)
		th = Thread(target=connected_users, args=(clientsocket,))
		th.start()
		print(th.getName())


def connected_users(clientsocket):
	try:
		while True:
			msg_time = datetime.datetime.now().strftime('%H:%M:%S')
			data = clientsocket.recv(BUF)
			username = (data.decode('utf-8')).split('>>')[0]
			messages = (data.decode('utf8'))
			recipient = ((data.decode('utf-8')).split('>>')[1]).split(':')[0]
			add_user(username, clientsocket)
			print(users)
			print("Data received {} {}".format(msg_time, messages))
			newdata = bytearray((msg_time + ' ' + messages), 'utf-8')

			if check_user(recipient):
				send_priv_message(recipient, newdata)
			elif ((data.decode('utf-8')).split('>>')[1]) == 'quit:':
				clientsocket.send((bytearray("quit", 'utf-8')))
				del users[username]
				print("<=Left chat", username)
				return False
			else:
				send_all(newdata, username)
	except IndexError:
		print('hren znaet ')



def add_user(username, clientsocket):
	if len(users) == 0:
		users[username] = clientsocket
	elif username in users and users.get(username):
		False
	else:
		users[username] = clientsocket

def send_all(newdata, username):
	for user, address in users.items():
		if username != user:
			address.send(newdata)
		else:
			pass


def send_priv_message(recipient, newdata):
	for user, address in users.items():
		if user == recipient:
				address.send(newdata)
		else:
			pass

def check_user(name):
		if users.get(name):
			return True
		else:
			return False




s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('localhost', 5060))


users = {}
BUF = 1024
if __name__ == "__main__":
	s.listen(5)
	print("Waiting for connection...")
	wait_connection()
s.close()