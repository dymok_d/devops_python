import socket
from threading import Thread
import datetime

shutdown = True

def get_message():
	global shutdown
	while shutdown:
		message = server.recv(BUF).decode('utf-8')
		if message != 'quit':
			print(message)
		else:
			shutdown = False
			break

def send_message():
		while shutdown:
			message = input(" > ")
			data = bytearray(name + '>>' + message, 'utf-8')
			server.send(data)






server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.connect(('localhost', 5060))
BUF = 1024

if __name__ == "__main__":
	name = input("Enter name: ")
	th1 = Thread(target=get_message)
	th1.start()
	#print(th1.getName())
	th2 = Thread(target=send_message)
	th2.start()
	#print(th2.getName())
else:
	server.close()