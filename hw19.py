import socket

local_hosts = {}

def start_server(ip, port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind((ip, port))
    print("UDP -Echo Server listening on port 5000:")
    while True:
        #print(local_hosts)
        data, address = server_socket.recvfrom(512)
        string_parse = (data.decode('utf-8')).replace('\n', '')
        resive = define_hosts(string_parse)
        #server_socket.sendto(bytearray(('If you wand add domain, use command ADD domain.com:1.1.1.1.1') + ('\n'), 'utf-8'), address)
        print(address, ":said", string_parse, resive)
        if resive == 'Host added':
            server_socket.sendto(bytearray(('Host added')+('\n'), 'utf-8'), address)
        elif resive == 'Host not found':
            server_socket.sendto(bytearray(('Host not found') + ('\n'), 'utf-8'), address)
        elif resive == 'Empty or domain not found in local dict':
            server_socket.sendto(bytearray(('Empty or domain not found in local dict') + ('\n'), 'utf-8'), address)
        else:
            for i in resive:
                encoderes = bytearray((i+('\n')), 'utf-8')
                server_socket.sendto(encoderes, address)

def check_local_host(str_parse):
    for i in local_hosts:
        if i == str_parse:
            return True

def define_hosts(string_parse):
    #print(string_parse)
    try:
        if string_parse.split()[0] == "ADD":
            host = (string_parse.split()[1]).split(':')[0]
            ip = (string_parse.split()[1]).split(':')[1]
            local_hosts.setdefault(host, [ip])
            return "Host added"
        else:
            #print("In local dict host not found")
            if any(local_hosts.keys()) == False:
                #print(1)
                return socket.gethostbyname_ex(string_parse)[2]
            elif check_local_host(string_parse) == True:
                #print(2)
                return local_hosts.get(string_parse)
            else:
                #print(3)
                return socket.gethostbyname_ex(string_parse)[2]
    except socket.gaierror:
        print("Host {} not found".format(string_parse))
        return "Host not found"
    except IndexError:
        print("Empty or domain not found in local dict")
        return "Empty or domain not found in local dict"



start_server('127.0.0.1', 5000)