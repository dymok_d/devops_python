#Пока создаем миниатюры только .jpg и .jpeg

import argparse
from PIL import Image

def create_thumbnail(filename, size):
	outfile = str(args.size+filename)
	try:
		im = Image.open(filename)
		im.thumbnail(size)
		im.save(outfile, "JPEG")
		return outfile
	except IOError:
		print ("Сannot create thumbnail for {}. File not found.".format(filename))
	except (IndexError, TypeError) as error:
		print('Enter correct size, example: 128x128')

def file_type (filename):
     if not filename.endswith((".jpg",".jpeg")):
         raise argparse.ArgumentTypeError("Only .jpg and .jpeg files allowed")
     return filename

def size_tuple (size):
	try:	
		t = tuple(map(int, size.split('x')))	
		return t
	except ValueError:
	 	print('Enter correct size, example: 128x128')
	 	exit(0)

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input", help="Enter -i|--input file.jpg or file.jpeg", type=file_type)
parser.add_argument("-s", "--size", help="Specify -s|--size 128x96")
args = parser.parse_args()
if args.input and args.size:
	filename = args.input
	sizen = size_tuple(args.size)
	print("Create thumbnail:", create_thumbnail(filename, sizen))
else:
	print("Please enter {} -i|--input file.jpg|.jpeg -s|--size width x height: Example {} -i jmg.jpg|.jpeg -s 123x123 ".format(parser.prog, parser.prog))