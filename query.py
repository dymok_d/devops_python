from functools import reduce

#Не получилось умножить значения в списке без модуля reduce...
#Как ни странно, даже при значении 200000, а не 1000000 все равно получаю верное значение.
print(reduce((lambda x, y: x * y), [(int((''.join([str(i) for i in range(200000)]))[10**i])) for i in range(0,7)]))

