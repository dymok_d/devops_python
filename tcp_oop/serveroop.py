import socket
from threading import Thread
import datetime


class Server:
    attr1 = 42
    CONNECTION_LIST = []
    RECV_BUFFER = 4096  # Advisable to keep it as an exponent of 2
    PORT = 5000

    def __init__(self):
        self.user_name_dict = {}
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_up_connections()
        self.client_connect()

    def set_up_connections(self):
        # this has no effect, why ?
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(("0.0.0.0", self.PORT))
        self.server_socket.listen(10)  # max simultaneous connections.
        self.CONNECTION_LIST.append(self.server_socket)

    def client_connect(self):
        print("Chat server started on port " + str(self.PORT))
        while True:
            clientsocket, clientaddress = self.server_socket.accept()
            msg_time = datetime.datetime.now().strftime('%H:%M:%S')
            data = clientsocket.recv(BUF)
            username = (data.decode('utf-8')).split('>>')[0]
            messages = (data.decode('utf8'))
            recipient = ((data.decode('utf-8')).split('>>')[1]).split(':')[0]
            add_user(username, clientsocket)
            print(users)
            print("Data received {} {}".format(msg_time, messages))

server = Server()

print(server.attr1)