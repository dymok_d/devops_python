from GPSPhoto import gpsphoto

def get_exif_photo (img):
	try:
		data = gpsphoto.getGPSData(img)
		coordinate = (data['Latitude'],data['Longitude'])
		return coordinate
	except KeyError:
		print('This photo does not have GPSData')
		exit(1)