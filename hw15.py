import ipaddress


class Router:
    def __init__(self):
        self.ip_list = []
        self.route_list = []

    def add_ip(self, interface, ip):
        ip_net = ipaddress.ip_network(ip, strict=False)
        ip_brd = str(ip_net.broadcast_address)
        ip_a = str(ipaddress.ip_interface(ip))

        if Router.check_arr(self.ip_list, ip_a):
            pass
        else:
            self.ip_list.append([str(interface), ip_a, ip_brd])
            router.add_route(ip_a, interface)

    def show_ip(self):
        print("*"*80)
        for i in self.ip_list:
            print("Interface {:7} IP and Mask {:20} broadcast {:20}".format(i[0], i[1], i[2]))

    def del_ip(self, interface, ip):
        ip_net = ipaddress.ip_network(ip, strict=False)
        ip_brd = str(ip_net.broadcast_address)
        ip_a = str(ipaddress.ip_interface(ip))
        a = 0
        for i in self.ip_list:
            if str(interface) in i and ip_a in i and ip_brd in i:
                del self.ip_list[a]
                router.del_route(ip_a, str(interface))
            a +=1

    def add_route(self, ip, interface):
        ip_net = str(ipaddress.ip_network(ip, strict=False))
        ip_a = str(ipaddress.ip_interface(ip).ip)
        if Router.check_arr(self.route_list, ip_a):
            pass
        else:
            self.route_list.append([ip_net, str(interface), ip_a])
            router.add_route(ip_a, interface)

    def show_route(self):
        print("*"*80)
        for i in self.route_list:
            print("Network {:20} interface {:7} ip {:20}".format(i[0], i[1], i[2]))

    def del_route(self, ip, interface):
        ip_net = ipaddress.ip_network(ip, strict=False)
        ip_a = str(ipaddress.ip_interface(ip).ip)
        a = 0
        for i in self.route_list:
            if str(interface) in i and ip_a in i:
                del self.route_list[a]
            a +=1

    @staticmethod
    def check_arr(arr, val):
        for i in arr:
            if val in i:
                return True
        return False


router = Router()


router.add_ip('eth0', '218.107.219.140/24')
router.add_ip('eth0', '218.107.34.11/24')
router.add_ip('eth0', '218.107.34.11/24')
router.add_ip('eth1', '192.168.0.103/24')
router.add_ip('eth5', '192.168.0.105/24')
router.del_ip('eth1', '192.168.0.103/24')

#router.add_route('192.168.0.0/24', 'eth0')
router.add_route('195.161.41.100/24', 'eth0')
router.add_route('195.161.41.0/24', 'eth0')
router.del_route('195.161.41.0/2', 'eth0')
router.show_ip()
router.show_route()