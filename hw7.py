"""
Напишите программу, которая читает данные из файлов
/etc/passwd и /etc/group на вашей системе и выводит
следующую информацию в файл output.txt:
1. Количество пользователей, использующих все имеющиеся
интерпретаторы-оболочки.
( /bin/bash - 8 ; /bin/false - 11 ; ... )
2. Для всех групп в системе - UIDы пользователей
состоящих в этих группах.
( root:1, sudo:1001,1002,1003, ...)
"""

#Функция для конвертации username в uid
def get_uid(username):
	with open("/etc/passwd") as file:
		for line in file:
			usern = line.split(':')[0]
			uid = line.split(':')[2]
			if username == usern:
				return uid

#Получение оболочек
a = {}
with open("/etc/passwd") as file:
	for line in file:
		sh = (line.split(':')[6]).replace('\n','')
		a[sh] = a.get(sh, 0) + 1

print("Print shels:")
for k,v in a.items():
	print("{} - {};".format(k,v), end=" ")
print("\n")
user = {}
uid_gid = {}


#Наверное не хорошо трижды читать один и тот же файл, попробую еще переделать...
with open("/etc/passwd") as file:
	for line in file:
		username = line.split(':')[0]
		uid = line.split(':')[2]
		gid = line.split(':')[3]
		user[username] = uid
		uid_gid[uid] = gid
gr = {}
#Все основное происходит здесь
with open("/etc/group") as file:
	for line in file:
		groupname = line.split(':')[0]
		gidg = line.split(':')[2]
		useringroup = line.rstrip('\n').split(':')[3:]
		result = [i for w in useringroup for i in w.split(',')]
		for items in uid_gid.items():
			if items[1] == gidg:
				gr.setdefault(groupname,[]).append(items[0])
		for i in result:
			if i != "": 
				gr.setdefault(groupname,[]).append(get_uid(i))

#форматирование без скобочек...
print("Group and UID:")
for k,v in gr.items():
	print("{}:{};".format(k, ','.join(v)), end=" ")