#problem9 - list comprehension : one line
#in process...

#Это решение я нашел на форуме, выглядит красиво
# import sys, itertools
#pif = [[a,b, sqrt(a**2 + b**2)] for a,b in combinations(range(1,1000),2) if 1000000-2000*a-2000*b+2*a*b==0]
#print(pif)


#Переписал, в list, работает
[print(x*y*((1000-x)-y)) for x in range(1,401) for y in range(1,401) if x < y < ((1000-x)-y) if x**2 + y**2 == ((1000-x)-y)**2]
 

#Сделал так
#for x in range(1, 400):
#	for y in range(1, 400):
#		z = (1000 - x) - y
#		if x < y < z:
#			if x**2 + y**2 == z**2:
#				print(x*y*z)



#problem6 - list comprehension : one line

print("Разница: {}".format((sum([i for i in range(1,101)])**2)-(sum([i*i for i in range(1,101)]))))

#problem48 - list comprehension : one line
print(str(sum([i**i for i in range(1,1001)]))[-10:])


#problem40 - list comprehension
#in process
#print([(int((''.join([str(i) for i in range(400000)]))[10**i])) for i in range(0,7)])

from functools import reduce

#Не получилось умножить значения в списке без модуля reduce...
#Как ни странно, даже при значении 200000, а не 1000000 все равно получаю верное значение.
print(reduce((lambda x, y: x * y), [(int((''.join([str(i) for i in range(200000)]))[10**i])) for i in range(0,7)]))


